//
// Created by Asraniel on 12.06.2021.
//

#ifndef CPPALGO_POINT_H
#define CPPALGO_POINT_H

#include <functional>
#include <unordered_set>
#include <ostream>
#include <random>

/**
 * Simple 2D point using doubles. The point can have an optional name.
 * The point implements basic operators to manipulate the point, as well as
 */

class Point {

public:
    Point() : x(), y() {};

    Point(Point &point) : x(point.x), y(point.y), name(point.name) {};

    Point(double x, double y, std::string name = "") : x(x), y(y), name(name) {};

    Point(const Point &point) : x(point.x), y(point.y), name(point.name) {}

    virtual double getX() const {
        return x;
    };

    virtual double getY() const {
        return y;
    };

    std::string getName() const {
        return name;
    };

    virtual void setX(double xNew) {
        this->x = xNew;
    };

    virtual void setY(double yNew) {
        this->y = yNew;
    };

    virtual double distance(const Point &point) const {
        double dist = (x - point.x) * (x - point.x) + (y - point.y) * (y - point.y);
        /*
        sqrt calculation:
        double counter=1,sqr=1;
        while (sqr<=dist){
            counter++;
            sqr= counter*counter;
        }
        counter-=1;
        */

        return sqrt(dist);
    };

    virtual Point &operator+=(const Point &other) {
        this->x += other.x;
        this->y += other.y;
        return *this;
    };

    virtual Point &operator-=(const Point &other) {
        this->x -= other.x;
        this->y -= other.y;
        return *this;
    };

    virtual Point &operator*=(double other) {
        this->x *= other;
        this->y *= other;
        return *this;
    };

    virtual Point &operator/=(double other) {
        if (other==0)throw std::domain_error("Division by zero");
        this->x /= other;
        this->y /= other;
        return *this;
    };

    virtual Point operator+(Point &other) {
        Point result(this->x,this->y,this->name);
        result.x = result.x + other.x;
        result.y = result.y + other.y;
        return result;
    };

    virtual Point operator-(Point &other) {
        Point result(this->x,this->y,this->name);
        result.x = result.x - other.x;
        result.y = result.y - other.y;
        return result;
    };

    virtual Point operator*(double other) {
        Point result(this->x,this->y,this->name);
        result.x = result.x * other;
        result.y = result.y * other;
        return result;
    };

    virtual Point operator/(double other) {
        if (other==0)throw std::domain_error("Division by zero");
        Point result(this->x,this->y,this->name);
        result.x = result.x / other;
        result.y = result.y / other;
        return result;
    };

    virtual bool operator==(const Point& other) const{
        return this->x==other.x&&this->y==other.y;
    }


    virtual bool operator!=(const Point& other) const{
        return this->x!=other.x&&this->y!=other.y;
    }



protected:
    double x, y;
    std::string name;
};

//Needs to be inline if defined in header
std::ostream &operator<<(std::ostream &os, const Point &point);

/**
 * Hashing class that allows to create a hash of a Point easily
 */
class PointHash {
public:
    std::size_t operator()(const Point *point) const {
        std::size_t h1 = std::hash<double>{}(point->getX());
        std::size_t h2 = std::hash<double>{}(point->getY());
        return h1 ^ (h2 << 1); // or use boost::hash_combine
    }
};

/**
 * Create a certain amount of random Points in a specified interval
 * @param count number of points to create
 * @param min minimum x and y value
 * @param max maximum x and y value
 * @param seed seed to be used to rng, -1 to not seed the rng
 * @return
 */
inline std::vector<Point *> createRandomPoints(size_t count, double min, double max, int seed = -1) {
    std::mt19937 random_engine;

    if (seed == -1) {
        std::random_device random_device;
        random_engine = std::mt19937(random_device());
    } else {
        random_engine = std::mt19937(seed);
    }
    std::uniform_int_distribution<int> distribution(min, max);

    std::unordered_set<Point *, PointHash> point_set;

    while (point_set.size() < count) {
        point_set.insert(new Point(distribution(random_engine), distribution(random_engine)));
    }

    return {point_set.begin(), point_set.end()};
}


static std::vector<Point> createRandomPoints2(int max, int min, size_t count, int seed=-1) {

    std::mt19937 random_engine;

    if (seed == -1) {
        std::random_device random_device;
        random_engine = std::mt19937(random_device());
    } else {
        random_engine = std::mt19937(seed);
    }
    std::uniform_int_distribution<int> distribution(min, max);

    std::unordered_set<Point *, PointHash> point_set;

    while (point_set.size() < count) {
        point_set.insert(new Point(distribution(random_engine), distribution(random_engine)));
    }
    std::vector<Point> randomPoints;

    for(Point* p:point_set){
        randomPoints.push_back(*p);
    }

    return randomPoints;
}


#endif //CPPALGO_POINT_H
