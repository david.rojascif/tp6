//
// Created by beatw on 7/14/2021.
//

#include "Clustering.h"

#include "TClosestPair.h"

#include <vector>

#include <random>
#include <numeric>
#include <algorithm>
#include <optional>
#include <iostream>

namespace Clustering {

    /**
     * Initialized cluster for KMeans algorithm
     *
     * Randomly selects one of the points to be the center of each cluster
     *
     * TODO: Implement pseudo code of
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> initKMeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {

        std::vector<Cluster> clusters;
        std::vector<bool> isPointAtIndexACluster(pts.size());

        std::mt19937 random_engine;

        if (seed == -1) {
            std::random_device random_device;
            random_engine = std::mt19937(random_device());
        } else {
            random_engine = std::mt19937(seed);
        }
        std::uniform_int_distribution<int> distribution(0, pts.size());

        while (nbOfClusters > 0 || clusters.size() == pts.size()) {
            int clusterIndex = distribution(random_engine);
            if (!isPointAtIndexACluster[clusterIndex]) {
                isPointAtIndexACluster[clusterIndex] = true;
                clusters.emplace_back(pts[clusterIndex]);
                nbOfClusters--;
            }
        }

        return clusters;
    }

    /**
     * Return the closest cluster for a given point.
     *
     *
     * @param clusters
     * @param p
     * @return
     */
    Cluster &closestCluster(std::vector<Cluster> &clusters, const Point &p) {
        return *(std::min_element(clusters.begin(), clusters.end(),
                                  [p](Cluster &cluster1, Cluster &cluster2) {
                                      return cluster1.distance(p) < cluster2.distance(p);
                                  }));
    }

    /**
     * Perform the KMeans clustering algorithm.
     *
     *
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> kmeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {
        std::vector<Cluster> clusters = Clustering::initKMeans(pts, nbOfClusters, seed);
        for(Cluster &cluster:clusters){
            cluster.clear();
        }
        Cluster closeCluster = closestCluster(clusters, *pts.begin());
        bool areClustersUnstable;
        //Assign points to clusters until the clusters are stable (do not move)
        do {
            for (const auto &pt: pts) {
                closeCluster = closestCluster(clusters, pt);
                auto cluster = std::find_if(clusters.begin(), clusters.end(),
                                            [closeCluster](Cluster &c) {
                                                return c == closeCluster;
                                            });//the clusters inside "clusters" won't update without doing this
                (*cluster).addPoint(pt);
            }
            areClustersUnstable = std::all_of(clusters.begin(), clusters.end(), [](Cluster &cluster)
            {return !cluster.recomputeCenter();});
        } while (areClustersUnstable);
        return clusters;
    }

    /**
     * Converts all points into clusters
     * @param pts
     * @return
     */
    std::vector<Cluster> initHierchical(const std::vector<Point> &pts) {
        std::vector<Cluster> clusters;
        for (const Point &point:pts) {
            clusters.emplace_back(Cluster(point));
        }
        return clusters;
    }

    /**
     * Performs a hierarchical clustering with nbOfCluster number of clusters.
     *
     * TODO: Use ClosestPair to find the closest clusters to merge
     * @param pts
     * @param nbOfClusters
     * @return
     */
    std::vector<Cluster> hierarchical(const std::vector<Point> &pts, size_t nbOfClusters) {
        std::vector<Cluster> clusters = initHierchical(pts);
        ClosestPair<Cluster> closestPair;

        //merge two the closest clusters until there are only nbOfClusters left
        while (clusters.size()!=nbOfClusters){
            auto closestClusters=closestPair.closestPair(clusters);
            for(const Point &p: closestClusters.second->getPoints()){
                closestClusters.first->addPoint(p);
            }
            closestClusters.first->recomputeCenter();
            clusters.erase(std::find_if(clusters.begin(),clusters.end(),[closestClusters](Cluster &cluster){
                return cluster==*closestClusters.second;
            }));
        }

        return clusters;
    }
}